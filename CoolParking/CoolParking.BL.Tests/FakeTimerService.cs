﻿using System.Timers;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;

namespace CoolParking.BL.Tests
{
    public class FakeTimerService :  ITimerService
    {
        public event ElapsedEventHandler Elapsed;

        private static Timer Timer { get; set; }
        private static Timer TimerForWriteLog { get; set; }

        public double Interval { get; set; }

        public FakeTimerService()
        {
            SetTimer();
            Elapsed += WithdrawTariff;
        }

        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);
        }

        public void Start()
        {
            Timer.Start();
        }

        public void Stop()
        {
            Timer.Stop();
            Dispose();
        }

        public void Dispose()
        {
            Timer.Dispose();
        }

        private static void SetTimer()
        {
            Timer = new Timer(Settings.PaymentRateSeconds);
            Timer.Elapsed += WithdrawTariff;
            Timer.AutoReset = true;
            Timer.Start();

            TimerForWriteLog = new Timer(Settings.WriteLogRateSeconds);
            TimerForWriteLog.Elapsed += TimerForWrite;
            TimerForWriteLog.AutoReset = true;
            TimerForWriteLog.Start();
        }

        private static void WithdrawTariff(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in Parking.vehicles)
            {
                vehicle.MinusBalance();
            }
        }

        private static void TimerForWrite(object sender, ElapsedEventArgs e)
        {
            LogService logService = new LogService(Settings.LogPath);
            string transactionString = "";

            ParkingService parking = new ParkingService();

            foreach (var transaction in parking.GetLastParkingTransactions())
                transactionString += transaction;

            logService.Write(transactionString);
        }
    }
}
