﻿using System.IO;
using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string logFilePath)
        {
            this.LogPath = logFilePath;
        }
        public string LogPath { get; } = Settings.LogPath;
        public void Write(string logInfo)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(LogPath, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(logInfo);
                }
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public string Read()
        {
            string logInfo = "";

            try
            {
                using (StreamReader sr = new StreamReader(LogPath))
                {
                    logInfo = sr.ReadToEnd();
                    Console.WriteLine(logInfo);
                }

                return logInfo;
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return logInfo;
        }
    }
}