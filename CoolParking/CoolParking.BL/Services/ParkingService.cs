﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService withdrawTimer;
        private readonly ITimerService logTimer;
        private readonly ILogService logService;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            this.logService = logService;
        }

        public ParkingService()
        {

        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }
        public int GetCapacity()
        {
            return Parking.Capacity;
        }
        public int GetFreePlaces()
        {
            return Parking.FreePlaces;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> vehicles = new ReadOnlyCollection<Vehicle>(Parking.vehicles);

            return vehicles;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (Parking.vehicles.Count == 0)
            {
                Parking.vehicles.Add(vehicle);
                Parking.FreePlacesDecrement();
            }
            else if (Parking.FreePlaces > 0)
            {
                if (FindVehicle(vehicle.Id) != -1)
                    throw new ArgumentException("Error! This ID is already there.");

                Parking.vehicles.Add(vehicle);
                Parking.FreePlacesDecrement();
            }
            else
                throw new InvalidOperationException("Error! Parking don`t has free place.");

        }
        private static int FindVehicle(string vehicleId)
        {
            if (Parking.vehicles.Count != 0)
            {
                for (int i = 0; i < Parking.vehicles.Count; i++)
                {
                    if (Parking.vehicles[i].Id == vehicleId)
                    {
                        return i;
                    }
                }
            }
            else
                return 0;

            return -1;
        }
        public void RemoveVehicle(string vehicleId)
        {
            int index = FindVehicle(vehicleId);

            if (index != -1)
            {
                if (Parking.vehicles[index].Balance < 0)
                    throw new InvalidOperationException("Error! This car owes.");
                else
                {
                    Parking.vehicles.RemoveAt(index);
                    Parking.FreePlacesIncrement();
                }
            }
            else
                throw new ArgumentException("Error! This ID doesn`t exist.");
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Error! A negative numver.");
            else
            {
                int index = FindVehicle(vehicleId);
                if (index != -1)
                {
                    Parking.vehicles[index].PlusBalance(sum);
                }
                else
                    throw new ArgumentException("Error! This ID doesn`t exist.");
            }
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            TransactionInfo[] transactionInfo = new TransactionInfo[Parking.transactionInfo.Count];

            for (int i = 0; i < Parking.transactionInfo.Count; i++)
            {
                transactionInfo[i] = Parking.transactionInfo[i];
            }

            return transactionInfo;
        }

        string IParkingService.ReadFromLog()
        {
            LogService logService = new LogService(Settings.LogPath);
            return logService.Read();
        }

        public void Dispose()
        {
            Parking.vehicles.Clear();
            Parking.transactionInfo.Clear();
            Parking.BalanceClear();
            Parking.CapacityClear();
            Parking.FreePlacesClear();
        }
    }
}