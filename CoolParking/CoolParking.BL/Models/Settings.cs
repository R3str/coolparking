﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Settings
    {
        public const decimal StartBalance = 0;
        public const int Capacity = 10;
        public const int PaymentRateSeconds = 5000;
        public const int WriteLogRateSeconds = 60000;

        public static Dictionary<VehicleType, decimal> PaymentForVehicleType = new Dictionary<VehicleType, decimal>()
        {
            [VehicleType.PassengerCar] = 2,
            [VehicleType.Bus] = 3.5M,
            [VehicleType.Truck] = 5,
            [VehicleType.Motorcycle] = 1
        };

        public const decimal PenaltyRate = 2.5M;

        public const string LogPath = "../../../LogInfo/LogInfo.txt";
    }
}