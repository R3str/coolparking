﻿using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public string Id;
        public decimal Sum;
        public DateTime Time;

        public TransactionInfo(string id, decimal sum, DateTime time)
        {
            this.Id = id;
            this.Sum = sum;
            this.Time = time;
        }
    }
}