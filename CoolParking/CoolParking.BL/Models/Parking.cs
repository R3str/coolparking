﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public static decimal Balance { get; private set; } = Settings.StartBalance;
        public static int Capacity { get; private set; } = Settings.Capacity;
        public static int FreePlaces { get; private set; } = Settings.Capacity;

        public static List<Vehicle> vehicles = new List<Vehicle>();
        public static List<TransactionInfo> transactionInfo = new List<TransactionInfo>();

        public static void AddToBalance(decimal payment)
        {
            Balance += payment;
        }

        public static void FreePlacesDecrement()
        {
            FreePlaces--;
        }
        public static void FreePlacesIncrement()
        {
            FreePlaces++;
        }

        public static void FreePlacesClear()
        {
            FreePlaces = Settings.Capacity;
        }
        public static void CapacityClear()
        {
            Capacity = Settings.Capacity;
        }
        public static void BalanceClear()
        {
            Balance = 0;
        }
    }
}