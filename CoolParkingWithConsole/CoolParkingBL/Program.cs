﻿using System;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParkingBL
{
    class Program
    {
        static void Main(string[] args)
        {
            LogService logService = new LogService(Settings.LogPath);
            TimerService timer = new TimerService();
            timer.FireElapsedEvent();

            ParkingService parking = new ParkingService();
            parking.AddVehicle(new Vehicle("AB-0123-QQ", VehicleType.Truck, 126));
            parking.AddVehicle(new Vehicle("CE-1298-VD", VehicleType.Bus, 1));
            parking.AddVehicle(new Vehicle("RE-7777-ER", VehicleType.PassengerCar, 490));
            parking.AddVehicle(new Vehicle("UY-0102-MN", VehicleType.Motorcycle, 228));

            bool doWork;

            while (true)
            {
                doWork = true;

                Console.WriteLine(
                    "Добро пожаловать в 'CoolParkinBL'!\n" +
                    "Что вам нужно?\n" +
                    "   1. Вывести информацию\n" +
                    "   2. Добавить/убрать Тр. средство\n" +
                    "   3. Пополнить баланс Тр. средства\n");

                string choise = Console.ReadLine();


                switch (choise)
                {
                    case "1":
                        GetInfo();

                        break;


                    case "2":
                        AddRemoveVehicles();

                        break;

                    case "3":
                        GetBalanceVehicle();

                        break;

                    default:
                        Console.WriteLine("Error!");
                        break;
                }

                Console.Clear();
            }


            void GetInfo()
            {
                while (doWork)
                {
                    Console.Clear();
                    Console.WriteLine(
                        "Вы в разделе 'Вывести информацию'\n" +
                        "Что ва нужно?\n" +
                        "   1. Баланс парковки\n" +
                        "   2. Cумма заработанных денег за текущий период\n" +
                        "   3. Сободные/занятые места на парковке\n" +
                        "   4. Все транзакции за текущий период\n" +
                        "   5. История транзакций (данные из файла Transactions.log)\n" +
                        "   6. Список тр. средств на парковке\n" +
                        "   7. Переход назад");
                    string choise = Console.ReadLine();


                    switch (choise)
                    {
                        case "1":
                            GetBalance();
                            break;

                        case "2":
                            EarnedMoneyForCurrentPeriod();
                            break;

                        case "3":
                            FreeOccupiedParkingSpaces();
                            break;

                        case "4":
                            AllTransactionsForCurrentPeriod();
                            break;

                        case "5":
                            TransactionHistory();
                            break;

                        case "6":
                            GetVehicles();
                            break;

                        case "7":
                            doWork = false;
                            break;

                        default:
                            Console.WriteLine("Error!");
                            break;
                    }
                }
                void GetBalance()
                {
                    Console.Clear();
                    Console.WriteLine("Баланс: " + parking.GetBalance());
                    JustOk();
                }
                void EarnedMoneyForCurrentPeriod()
                {
                    Console.Clear();

                    decimal sum = 0;

                    foreach (var transaction in parking.GetLastParkingTransactions())
                        sum += transaction.Sum;

                    Console.WriteLine("Сумма заработанных денег за текущий период: " + sum);

                    JustOk();
                }
                void FreeOccupiedParkingSpaces()
                {
                    Console.Clear();
                    Console.WriteLine("Свободные места: " + Parking.FreePlaces);

                    Console.WriteLine("Занятые места: " + (Parking.Capacity - Parking.FreePlaces));

                    JustOk();
                }
                void AllTransactionsForCurrentPeriod()
                {
                    Console.Clear();

                    foreach (var transaction in parking.GetLastParkingTransactions())
                        Console.WriteLine(transaction);

                    JustOk();
                }
                void TransactionHistory()
                {
                    Console.Clear();

                    Console.WriteLine(logService.Read());

                    JustOk();
                }
                void GetVehicles()
                {
                    Console.Clear();
                    foreach (Vehicle el in parking.GetVehicles())
                    {
                        Console.WriteLine(el);
                    }

                    JustOk();
                }
            }
            void AddRemoveVehicles()
            {
                while (doWork)
                {
                    Console.Clear();
                    Console.WriteLine(
                        "Вы в разделе 'Добавить/убрать Тр. средство'\n" +
                        "Что вам нужно?\n" +
                        "   1. Добавить Тр. средство вручную\n" +
                        "   2. Добавить Тр. средство используя случайно подобранные ID\n" +
                        "   3. Убрать Тр. средство\n" +
                        "   4. Переход назад");

                    string choise = Console.ReadLine();

                    string id = "";
                    string vehicleType = "";
                    string balance = "";

                    switch (choise)
                    {
                        case "1":
                            Console.Clear();

                            Console.Write("Впишите ID формата 'ХХ-YYYY-XX' (X - буква, Y - цифра): ");
                            id = Console.ReadLine();

                            Console.WriteLine(
                                "Выбирете тип Тр. средства:\n" +
                                "   1. Легковое" +
                                "   2. Грузовое" +
                                "   3. Автобус" +
                                "   4. Мотоцикл");
                            vehicleType = Console.ReadLine();

                            Console.WriteLine(
                                "Впишите баланс Тр. средства:");
                            balance = Console.ReadLine();

                            try
                            {
                                parking.AddVehicle(new Vehicle(id, (VehicleType)(Int32.Parse(vehicleType) - 1), Decimal.Parse(balance)));
                                Console.WriteLine(parking.GetVehicles()[Parking.vehicles.Count - 1]);
                            }
                            catch
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Ошибка!");
                            }

                            JustOk();
                            break;

                        case "2":
                            Console.Clear();

                            Console.WriteLine(
                                "Выбирете тип Тр. средства:\n" +
                                "   1. Легковое" +
                                "   2. Грузовое" +
                                "   3. Автобус" +
                                "   4. Мотоцикл");
                            vehicleType = Console.ReadLine();

                            Console.WriteLine(
                                "Впишите баланс Тр. средства:");
                            balance = Console.ReadLine();

                            try
                            {
                                parking.AddVehicle(new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), (VehicleType)(Int32.Parse(vehicleType) - 1), Decimal.Parse(balance)));

                                Console.WriteLine(parking.GetVehicles()[Parking.vehicles.Count - 1]);
                            }
                            catch
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Ошибка!");
                            }

                            JustOk();
                            break;

                        case "3":
                            Console.Clear();

                            Console.Write("Впишите ID формата 'ХХ-YYYY-XX' (X - буква, Y - цыфра): ");
                            id = Console.ReadLine();

                            try
                            {
                                Console.WriteLine("\nDeleted this vehicle:" + Parking.vehicles[parking.FindVehicle(id)]);
                                parking.RemoveVehicle(id);
                            }
                            catch
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Ошибка!");
                            }

                            JustOk();
                            break;

                        case "4":
                            doWork = false;
                            break;

                        default:
                            Console.WriteLine("Error!");
                            break;
                    }
                }
            }
            void GetBalanceVehicle()
            {
                Console.Clear();
                Console.Write(
                    "Вы в разделе 'Пополнить баланс Тр. средства'\n" +
                    "Впишите ID нужного вам тр. средства: ");
                string id = Console.ReadLine();

                try
                {
                    parking.FindVehicle(id);

                    Console.WriteLine("\nВыбранное тр. средство:" + Parking.vehicles[parking.FindVehicle(id)]);

                    Console.Write("\nДобавить к балансу тр. средства: ");
                    string sum = Console.ReadLine();

                    parking.TopUpVehicle(id, Convert.ToDecimal(sum));
                    Console.WriteLine("\nВыбранное тр. средство c новым балансом:" + Parking.vehicles[parking.FindVehicle(id)]);
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ошибка!");
                }

                JustOk();
            }

            void JustOk()
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("\n   Ok");
                Console.ResetColor();
                Console.ReadLine();
            }
        }
    
    }
}
