﻿using System;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public static string idS { get; private set; }
        VehicleType VenichleType { get; }
        public decimal Balance { get; private set; }

        public Vehicle(string id, VehicleType venichleType, decimal balance)
        {
            this.Id = CheckId(id);
            this.VenichleType = venichleType;
            this.Balance = balance <= 0 ? throw new ArgumentException("Error") : balance;
        }
        public Vehicle()
        {
            this.Id = "AA-0000-AA";
            this.VenichleType = VehicleType.PassengerCar;
            this.Balance = 0;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var rand = new Random();

            char[] idChar = new char[8];

            for (int i = 0; i < 8; i++)
            {
                // Add litteras and numbers
                if (i < 2 || i > 5)
                    idChar[i] = (char)rand.Next(65, 91);
                else
                    idChar[i] = (char)rand.Next(48, 58);
            }

            idS = new string(idChar);

            idS = idS.Insert(2, "-")
                     .Insert(7, "-");

            return idS;
        }

        public string CheckId(string id)
        {
            // Chechk length
            if (id.Length != 10)
            {
                //return "Error";
                throw new ArgumentException("Error");
            }
            // Check 2 symbols '-'
            if (id[2] != '-' || id[7] != '-')
            {
                //return "Error";
                throw new ArgumentException("Error");
            }

            for (int i = 0; i < 10; i++)
            {
                if (i == 2 || i == 7)
                    continue;

                // Check litteras at 1,2 and 8, 9 positions
                else if (i < 2 || i > 7)
                {
                    if ((int)id[i] < 65 || (int)id[i] > 122)
                    {
                        //return "Error";
                        throw new ArgumentException("Error");
                    }
                }
                // Check numbers at other position 
                else if ((int)id[i] < 48 || (int)id[i] > 59)
                {
                    //return "Error";
                    throw new ArgumentException("Error");
                }
            }

            return id.ToUpper();
        }

        public void MinusBalance()
        {
            decimal payment;

            Settings.PaymentForVehicleType.TryGetValue(VenichleType, out payment);

            if (Balance >= payment)
                Balance -= payment;
            else if (Balance <= 0)
            {
                payment = payment * Settings.PenaltyRate;

                Balance -= payment;
            }
            else
            {
                payment = Balance + (payment - Balance) * Settings.PenaltyRate;

                Balance -= payment;
            }

            Parking.AddToBalance(payment);

            Transaction(Id, payment, DateTime.Now);
        }

        public void PlusBalance(decimal value)
        {
            Balance += value;
        }

        private void Transaction(string id, decimal sum, DateTime time)
        {
            Parking.transactionInfo.Add(new TransactionInfo(id, sum, time));
        }

        public override string ToString()
        {
            string result = "\n Id: " + Id + "\n Type: " + VenichleType + "\n Balance: " + Balance;

            return result;
        }
    }
}
